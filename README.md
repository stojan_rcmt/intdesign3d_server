Info
=================

This is NodeJS server for 3D interactive design. For server is used Express framework, for HTML templating Mustache. All 3D objects are ommited from this revision history, because they are usually binaries and to big.

Installation
----------------

    $ npm install

Run
----------------

    $ npm run start

or directly:

    $ node src/index.js

By default server is running on port 3000. To run server on different port set extra -p=X parameter, where X is port number. For example if you want to run server on port 3001 run:

    $ node src/index.js -p 3001

Build
----------------
Server serves static files from ``public`` folder. Templates for static pages are in ``src/templates`` folder. Client-side Javascript app is located in

    public/js/index.js

file. If this project is on the same parent folder as client side (`intdesign3d`) project, you can build final, production ready script from this project with:

    $ npm run buildServer
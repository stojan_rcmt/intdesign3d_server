module.exports = {
    name: "3dIntegratedDesign",
    script: "./src/index.js",
    env: {
        NODE_ENV: "development"
    },
    env_production: {
        NODE_ENV: "production"
    }
}
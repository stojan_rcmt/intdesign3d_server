/**
 * User manipulation - get, register etc
 */
const { users } = require('../helpers/Users');

module.exports = (function UserController() {
  return {
    /**
     * Check if username/pass are correct return that user
     */
    getUser: (username, pass) => {
      const lUsername = username ? username.toLowerCase() : username;
      for (let i = 0; i < users.length; i += 1) {
        if (users[i].username.toLowerCase() === lUsername) {
          if (users[i].password === pass) {
            return users[i];
          }
          return null;
        }
      }
      return null;
    },
  };
}());

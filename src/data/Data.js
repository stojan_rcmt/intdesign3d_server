/**
 * Global data/model "class"
 */

const models = require('../helpers/Models');
const { ROLES } = require('../helpers/Users');

module.exports = {
  /**
   * return 3D model config object, based on his id
   */
  getModel: (id) => {
    let i;

    for (i = 0; i < models.length; i += 1) {
      if (models[i].id === id) {
        return models[i];
      }
    }
    return null;
  },
  /**
   * get user role name (used for templated) based on role ID
   * @returns {String} - user role name
   */
  getRole: (id) => {
    let roleName;

    switch (id) {
      case ROLES.FINANCE:
        roleName = 'finance';
        break;
      case ROLES.ENGINEER:
        roleName = 'engineer';
        break;
      default: roleName = 'admin';
    }

    return roleName;
  },
};

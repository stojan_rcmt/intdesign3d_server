/**
 * Parse input parameters to get all arguments in hash map
 */

const args = {};

if (process.argv.length > 1) {
  for (let i = 0; i < process.argv.length; i += 1) {
    const cParameter = process.argv[i];
    if (cParameter.charAt(0) === '-') {
      if (cParameter.length > 0) {
        if (cParameter.length > 2) {
          args[cParameter.charAt(1)] = cParameter.slice(2);
        } else {
          try {
            args[cParameter.charAt(1)] = process.argv[i + 1];
            i += 1;
          } catch (e) {
            // Catch error
          }
        }
      }
    }
  }
}

module.exports = args;

/**
 * Hardcoded array of all available 3D models with some properties
 */
module.exports = [
  {
    id: 1,
    file: '../models/Elementi.gltf',
    properties: {
    },
  },
  {
    id: 2,
    file: '../models/Road.gltf',
    properties: {
    },
  },
  {
    id: 3,
    file: '../models/Stojan.gltf',
    properties: {
      dangerous: [
        'CB|1|TUBE',
        'CB|2|TUBE',
        'CB|1|EQUIPMENT',
      ],
      clicks: {
        /* 'CB|1|BUSHING': {
          type: 'gallery',
          data: [
            'documents/model3/circuit_breaker_type_2/IMG_1195.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1204.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1207.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1208.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1209.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1210.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1211.JPG',
            'documents/model3/circuit_breaker_type_2/IMG_1212.JPG',
          ],
        }, */
        'CB|{number}|STRUCTURE': {
          type: 'pdf',
          data: 'documents/model3/cb_structure.pdf',
        },
        'CB|{number}|EQUIPMENT': {
          type: 'pdf',
          data: 'documents/model3/BOU.01.009.00.001-1.1.pdf',
        },
        'CB|{number}|BUSHING': {
          type: 'pdf',
          data: 'documents/model3/BOU.01.009.00.001-1.1.pdf',
        },
        'CB|{number}|TUBE': {
          type: 'pdf',
          data: 'documents/model3/CB_1_TUBE.pdf',
        },
        'CB|{number}|CLAMP': {
          type: 'pdf',
          data: 'documents/model3/CB_1_CLAMP.pdf',
        },
        'CB|{number}|FLEX_CONNECTOR': {
          type: 'pdf',
          data: 'documents/model3/CB_1_CLAMP.pdf',
        },
      },
    },
  },
  {
    id: 4,
    file: '../models/StojanClickable.gltf',
    properties: {
      sprites: {
        finance_001: 'documents/Invoice_House.pdf',
        finance_002: 'documents/Invoice_Template.pdf',
      },
      timeline: [
        {
          date: '1/5/2018',
          expected: [
            'Layer25_PSR_Ground',
            'Layer24_PSR_Trench_3d',
          ],
          finished: [
            'Layer25_PSR_Ground',
          ],
        },
        {
          date: '3/1/2018',
          expected: [
            'Layer11_PSR_Wall_3d',
          ],
          finished: [
            'Layer24_PSR_Trench_3d',
            'Layer11_PSR_Wall_3d',
          ],
        },
        {
          date: '5/1/2018',
          expected: [
            'Layer2_PSR_Structures_3d',
          ],
          finished: [],
        },
        {
          date: '7/1/2018',
          expected: [
            'Layer1_PSR_Equipment bushing_3d',
          ],
          finished: [
            'Layer2_PSR_Structures_3d',
            'Layer1_PSR_Equipment bushing_3d',
          ],
        },
      ],
    },
  },
  {
    id: 5,
    file: '../models/Nemanja.gltf',
    properties: {
    },
  },
];

/**
 * Users model
 */
const roles = {
  ADMIN: 1,
  FINANCE: 5,
  ENGINEER: 6,
};
const permissions = {
  MOVING: 0b01,
  SELECTING: 0b10,
};

module.exports = {
  /**
   * List of all hardcoded users with parameters
   */
  users: [
    {
      username: 'psr_serbia',
      password: '1234',
      role: roles.FINANCE,
      permission: permissions.MOVING,
      model: 1,
    },
    {
      username: 'rcmt_canada',
      password: '1234',
      role: roles.ENGINEER,
      permission: permissions.MOVING | permissions.SELECTING,
      model: 2,
    },
    {
      username: 'stojan',
      password: '1234',
      role: roles.ENGINEER,
      permission: permissions.MOVING | permissions.SELECTING,
      model: 3,
    },
    {
      username: 'clickable',
      password: '1234',
      role: roles.FINANCE,
      permission: permissions.MOVING,
      model: 4,
    },
    {
      username: 'nemanja',
      password: '1234',
      role: roles.ENGINEER,
      permission: permissions.MOVING | permissions.SELECTING,
      model: 5,
    },
  ],
  /**
   * Hashmap of user Roles
   */
  ROLES: roles,
  /**
   * Hashmap of user permissions
   */
  PERMISSIONS: permissions,
  /**
   * Check if current role have some permission
   * @param {Number} role - role permission number
   * @param {Number} permission - check against that permission
   * @returns {Boolean} - true if user have that permission
   */
  checkPermission: (role, permission) => {
    if (role & permission) {
      return true;
    }
    return false;
  },
};

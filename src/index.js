// Server libraries
const express = require('express');
const bodyParser = require('body-parser');
const mustacheExpress = require('mustache-express');

// our classes
const args = require('./helpers/ArgumentsParser');
const users = require('./controllers/UserController');
const usersHardcoded = require('./helpers/Users');
const Data = require('./data/Data');
const { ROLES } = require('./helpers/Users');

// DEFAULT PARAMS
let port = 3000;

// update default parameters from arguments
if (args) {
  if (Object.prototype.hasOwnProperty.call(args, 'p')) {
    port = args.p;
  }
}

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

app.engine('html', mustacheExpress());
app.set('view_engine', 'mustache');
app.set('views', `${__dirname}/templates`);

app.all('/', (req, res) => {
  const cTemplateVars = {};
  if (req.body.username !== undefined) {
    const user = users.getUser(req.body.username, req.body.pass);

    if (user) {
      const filteredUser = Object.assign({}, user);
      delete filteredUser.password;

      res.render('home.html', {
        username: user.username,
        role: Data.getRole(user.role),
        isEngineer: user.role === ROLES.ENGINEER,
        isFinance: user.role === ROLES.FINANCE,
        data: JSON.stringify({
          user: filteredUser,
          model: Data.getModel(user.model),
        }),
      });
      return;
    }
    cTemplateVars.error = 'Wrong username or password entered!';
  }

  // user not found
  const hcUsers = [];
  usersHardcoded.users.forEach((user) => {
    let cRole = 'Administrator';
    switch (user.role) {
      case usersHardcoded.ROLES.ENGINEER:
        cRole = 'Engineer';
        break;
      case usersHardcoded.ROLES.FINANCE:
        cRole = 'Finance';
        break;
      default: cRole = 'Administrator';
    }
    hcUsers.push({
      username: user.username,
      password: user.password,
      role: cRole,
    });
  });

  cTemplateVars.users = hcUsers;

  res.render('login.html', cTemplateVars);
});

app.all('/help', (req, res) => {
  res.render('help.html');
});

app.listen(port);
